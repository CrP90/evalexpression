import java.util.Scanner;
import java.util.Stack;

public class EvalExpr {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		System.out.println("Enter expression \n>>");
		String expr = Expressions.infixToPostfix(in.nextLine());
		System.out.println(evalExpr(expr));
		
	}
	
	static Rational evalExpr(String str){
		Stack<Rational> stack = new Stack<Rational>();
		for (int i = 0; i < str.length(); i++){
			char ch = str.charAt(i);
			if (ch >= '0' && ch <= '9'){
				i = getNr(stack, str, i);
			}
			else if (ch == '+' || ch == '-' || ch == '/' || ch == '*' || ch == '%'){
				Rational op2 = stack.pop();
				Rational op1 = stack.pop();
				stack.push(do_op(op1, op2, ch));
			}
			else if (ch == '.'){
				continue;
			}
		}
		return (stack.pop());
	}

	static int getNr(Stack<Rational> stack, String str, int i){
		String result = "";
		char ch;
		while (i < str.length() && ((ch = str.charAt(i)) >= '0' && ch <= '9')){
			result += ch;
			i++;
		}
		stack.push(new Rational(Integer.parseInt(result)));
		return (i - 1);
	}

	static Rational do_op(Rational a, Rational b, char c){
		switch(c){
			case '+' :
				return (a.add(b));
			case '-' :
				return (a.subtract(b));
			case '*' :
				return (a.multiply(b));
			default:
				return (a.divide(b));
		}
	}
}
