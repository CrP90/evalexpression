public class Rational {

	private int num;
	private int den;
	
	public Rational() {
		this(0);
	}
	
	public Rational(int num) {
		this(num, 1);
	}
	
	public Rational(int num, int den) throws IllegalArgumentException {
		if (den == 0)
			throw new IllegalArgumentException("Division by 0");
		int g = gcd(Math.abs(num), Math.abs(den));
		this.num = num / g;
		this.den = den / g;
		if (den < 0)
			this.num = -num;
	}
	
	public Rational add(Rational that) {
		return (new Rational(this.num * that.den + this.den * that.num, this.den * that.den));
	}
	
	public Rational subtract(Rational that) {
		return (new Rational(this.num * that.den - this.den * that.num, this.den * that.den));
	}
	
	public Rational multiply(Rational that) {
		return (new Rational(this.num * that.num, this.den *  that.den));
	}
	
	public Rational divide(Rational that) {
		return (new Rational(this.num * that.den, this.den *  that.num));
	}
	
	@Override
	public String toString() {
		if (this.den == 1)
			return ("" + this.num);
		double result = (double)num / (double)den;
		return (this.num + "/" + this.den + "(" + result + ")");
	}

	private int gcd(int a, int b) {
		int r = a % b;
		while (r != 0) {
			a = b;
			b = r;
			r = a % b;
		}
		return (b);
	}
	
}
