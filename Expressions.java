import java.util.*;

public final class Expressions {

	public static String infixToPostfix(String expr) {
		
		StringBuilder str = new StringBuilder();
		Stack<Character> stack = new Stack<Character>();
		
		for (int i = 0; i < expr.length(); i++) {
			char ch = expr.charAt(i);
			if (ch == ' '){
				continue;
			}
			else if (isOperator(ch)) {
				while (!stack.isEmpty() && stack.peek() != '(' && hasHigherPrecedence(stack.peek(), ch)){
					str.append(stack.pop());
				}
				stack.push(ch);
			}
			else if (ch >= '0' && ch <= '9') {
				str.append(ch);
				if ((i + 1) < expr.length() && (expr.charAt(i + 1) < '0' || expr.charAt(i + 1) > '9'))
					str.append('.');
			}
			else if (ch == '(')
				stack.push(ch);
			else if (ch == ')'){
				while (!stack.isEmpty() && stack.peek() != '(')
					str.append(stack.pop());
				if (!stack.isEmpty())
					stack.pop();
			}
		}
		while (!stack.isEmpty())
			str.append(stack.pop());
		return (str.toString());
	}
	
	private static boolean isOperator(char ch) {
		return (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '%');
	}
	
	private static boolean hasHigherPrecedence(char ch1, char ch2) {
		byte op1 = getOperatorHeight(ch1);
		byte op2 = getOperatorHeight(ch2);
		return (op1 >= op2);
	}
	
	private static byte getOperatorHeight(char c) {
		if (c == '+' || c == '-')
			return (1);
		return (2);
	}
	
	
}
