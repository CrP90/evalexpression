EvalExpression can solve basic mathematical expressions and returns the result
as a Rational number. The algorithm use Infix to Postfix conversion. Evaluation
of Postfix expression is made using stack.

A valid expression:

-must only contain the operators +, -, * and /;

-must only have integer values;

-can contain parentheses, but each group must be properly closed;

-can contain spaces.
